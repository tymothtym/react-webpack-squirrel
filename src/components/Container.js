import React from "react";

const Title = () => (
   	<div className="columns">
   		<div className="column col-12">
  			<h6 className='title text-center'><em>React webpack squirrel: test page</em></h6>
		</div>
    </div>
);

const Container = props => (
  	<div className="container mt-2 pt-2">
      	<Title />
        {props.children}
  	</div>
);


export default Container;
