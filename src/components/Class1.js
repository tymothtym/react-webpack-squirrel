import React, {Component} from 'react';
import logoIcon from '../assets/images/squirrel_icon.png';

class Class1 extends Component {

  constructor() {
    super();

  }
  hello = () => {
    return (
    	<React.Fragment><h1>Squirrel!</h1></React.Fragment>);
  }

  render() {
    return (
       	<div className="columns">
			<div className="column col-6 col-md-12 is-center-self text-center">
		   		<img className="is-responsive" src={logoIcon}/>
		   	</div>
       		<div className="column col-6 col-md-12 is-center-self text-center">
                <h3>Hello</h3>
                {this.hello()}
		    </div>
		</div>
    );
  }
}
export default Class1;
