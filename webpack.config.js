//plugins
const TerserJSPlugin = require('terser-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

// environmental variables
const path = require('path');
const mode = process.env.NODE_ENV || 'development';

// options
const entry = {'index':'./src/app.js'};
const output = {
    filename: 'bundle.js',
    path: path.resolve(__dirname, './public')
};

module.exports = {
  mode,
  entry,
  output,
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    port: 9000
  },
  devtool: 'source-map',
  optimization: {
    minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
  },
  plugins: [
    new HtmlWebPackPlugin({template: './src/index.html'}),
    new MiniCssExtractPlugin({
        filename: 'app.bundle.css'
    }),
    new CleanWebpackPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }, {
        test: /\.(s*)css$/,
        exclude: /node_modules/,
        use: [
          mode !== 'production' ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader']
      }, {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              minimize: true
            }
          }
        ]
      }, { 
      	test: /\.(jpe?g|gif|png|svg|woff|ttf|wav|mp3)$/,
        exclude: /node_modules/,
      	use: [
          {
          	loader: 'file-loader',
            options: {
              //outputPath: path.resolve(__dirname, './public'),
              //name: './assets/[name].[ext]'
              name: './assets/[name].[ext]'
              //name: path.resolve(__dirname, 'assets') + '[name].[ext]'
            }
          }
        ]
      }
    ]
  }
};
