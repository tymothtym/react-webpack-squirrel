# react-webpack-squirrel
Webpack 4 (+ dev server) with React, ES6 binding features and SASS with Spectre.css framework applied.

Meant for front-end use.

Tested by squirrels.

## Clone

```git clone https://tymothtym@bitbucket.org/tymothtym/react-webpack-squirrel.git [your-project-name-here]```

## To install

```cd [path/to/your-project-name-here]```
```npm install```

## To develop

```src``` folder contains all app development.

```npm run dev```

This compiles the app according to ```webpack.config.js``` and runs webpack-dev-server on ```http://localhost:9000/```

## To compile / build

```npm run build```

This compiles the app according to ```webpack.config.js``` and outputs the app to ```public``` folder

CSS is autoprefixed and minified in production
